var questions = [
    {
        "question": "Έχει Κόκκινα Μαλλιά?",
        "key": "HasRedHair"
    },
    {
        "question": "Έχει Μάυρα Μαλλιά?",
        "key": "HasBlackHair"
    },
    {
        "question": "Έχει Ξανθά Μαλλιά?",
        "key": "HasBlondeHair"
    },
    {
        "question": "Έχει Γκρίζα Μαλλιά?",
        "key": "HasGrayHair"
    },
    {
        "question": "Είναι Αγόρι?",
        "key": "IsABoy"
    },
    {
        "question": "Είναι Κορίτσι?",
        "key": "IsAGirl"
    },
    {
        "question": "Φοράει Καπέλο?",
        "key": "WearsAHat"
    },
    {
        "question": "Έχει Σκουλαρίκια?",
        "key": "HasEarings"
    },
    {
        "question": "Φοράει Γυαλιά?",
        "key": "WearsGlasses"
    },
    {
        "question": "Έχει Μούσι?",
        "key": "HasBeard"
    },
    {
        "question": "Έχει Μουστάκι?",
        "key": "HasMustache"
    },
    {
        "question": "Έχει Μακριά Μαλλιά?",
        "key": "HasLongHair"
    },
    {
        "question": "Έχει Καρφάκια ?",
        "key": "HasSpikyHair"
    },
    {
        "question": "Έχει Σγουρά Μαλλιά?",
        "key": "HasCurlyHair"
    },
    {
        "question": "Έχει Ισια Μαλλιά?",
        "key": "HasStraightHair"
    },
    {
        "question": "Καθόλου Μαλλιά?",
        "key": "HasNoHair"
    },
    {
        "question": "Έχει Μικρά μάτια?",
        "key": "HasSmallEyes"
    },
    {
        "question": "Έχει Μεγάλα μάτια?",
        "key": "HasBigEyes"
    },
    {
        "question": "Έχει Μεγάλα χείλη?",
        "key": "HasBigLips"
    },
    {
        "question": "Είναι Σοβαρός?",
        "key": "IsSerious"
    },
    {
        "question": "Είναι Λυπημένος?",
        "key": "IsSad"
    },
    {
        "question": "Είναι Χαρούμενος?",
        "key": "IsHappy"
    },
    {
        "question": "Έχει Τετράγωνο Πίγουνι?",
        "key": "HasSquareChin"
    },
    {
        "question": "Έχει Στρογγυλό Πίγουνι?",
        "key": "HasRoundChin"
    },
    {
        "question": "Έχει Θεληματικό Πίγουνι?",
        "key": "HasNiceChin"
    }
];