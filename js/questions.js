var questions = [
    {
        "question": "Has Red Hair?",
        "key": "HasRedHair"
    },
    {
        "question": "Has Black Hair?",
        "key": "HasBlackHair"
    },
    {
        "question": "Has Blonde Hair?",
        "key": "HasBlondeHair"
    },
    {
        "question": "Has Gray Hair?",
        "key": "HasGrayHair"
    },
    {
        "question": "Is A Boy?",
        "key": "IsABoy"
    },
    {
        "question": "Is A Girl?",
        "key": "IsAGirl"
    },
    {
        "question": "Wears A Hat?",
        "key": "WearsAHat"
    },
    {
        "question": "Has Earings?",
        "key": "HasEarings"
    },
    {
        "question": "Wears Glasses?",
        "key": "WearsGlasses"
    },
    {
        "question": "Has Beard?",
        "key": "HasBeard"
    },
    {
        "question": "Has Mustache?",
        "key": "HasMustache"
    },
    {
        "question": "Has Long Hair?",
        "key": "HasLongHair"
    },
    {
        "question": "Has Spiky Hair?",
        "key": "HasSpikyHair"
    },
    {
        "question": "Has Curly Hair?",
        "key": "HasCurlyHair"
    },
    {
        "question": "Has Straight Hair?",
        "key": "HasStraightHair"
    },
    {
        "question": "Has No Hair?",
        "key": "HasNoHair"
    },
    {
        "question": "Has Small Eyes?",
        "key": "HasSmallEyes"
    },
    {
        "question": "Has Big Eyes?",
        "key": "HasBigEyes"
    },
    {
        "question": "Has Big Lips?",
        "key": "HasBigLips"
    },
    {
        "question": "Is Serious?",
        "key": "IsSerious"
    },
    {
        "question": "Is Sad?",
        "key": "IsSad"
    },
    {
        "question": "Is Happy?",
        "key": "IsHappy"
    },
    {
        "question": "Has Square Chin?",
        "key": "HasSquareChin"
    },
    {
        "question": "Has Round Chin?",
        "key": "HasRoundChin"
    },
    {
        "question": "Has Nice Chin?",
        "key": "HasNiceChin"
    }
];