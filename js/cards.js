function Card(properties, index) {

    var $this = this;
    var props = properties;
    this.properties = properties;
    var htmlTemplate = "<div class='card'><img class='cardImg' src='assets/" + props.img0 + "' /><img class='cardBG' src='assets/card.png' /><div class='cardName'>" + props.name + "</div></div>";

    this.createHTML = function () {

        return {
            html: htmlTemplate,
            item: this
        };

    };

    this.checkProperty = function (prop) {
        console.log(prop, this.properties[prop]);
        if (this.properties[prop] === false) {
            return false;
        } else {
            return true;
        }
    };

    return this;
};