function AI() {

    var $this = this;
    this.selectedImage;
    this.selectedImageFile;

    this.selectImage = function () {
        var item1 = Math.abs(getRandom(25, 0));
        var item2 = Math.abs(getRandom(25, 0));
        var item3 = Math.abs(getRandom(15, 10));
        var item4 = Math.abs(getRandom(25, 10));
        var item5 = Math.abs(getRandom(5, 0));
        var itemMax = Math.max(item1, item2, item3, item4, item5);
        var itemMin = Math.min(item1, item2, item3, item4);
        console.log(itemMax, itemMin);

        var item = Math.abs(getRandom(25, itemMin));

        this.selectedImage = item;
        this.selectedImageFile = "img" + item + ".png";
        return "img" + item + ".png";
    };

    this.getSelectedImage = function () {
        return this.selectedImage;
    };

    this.getSelectedImageFile = function () {
        return this.selectedImageFile;
    };

    this.answerQuestion = function (query) {

        var _query = String(query).replace(/[\s\?]/g, '');
        console.log(query, _query);
        var response = properties[this.selectedImage][_query];

        return response;

    };

    /** * getRandom * Description
     * @param {type} max* Description
     * @param {type} min* Description
     **/
    function getRandom(max, min) {
        return Math.floor(Math.random() * (1 + min - max) + min);
    }

    return this;
};