$(document).ready(function () {

    $("#lean_overlay").css({
        'display': 'block',
        'opacity': 1
    });
    TweenMax.to("#lean_overlay", 1.2, {
        "opacity": 0.0,
        onComplete: function () {
            $("#lean_overlay").css("display", "none");
        }
    });

    createCards();
    createQuestions();
    createThrowprops();

    AddEventListeners();

    /// AI ////
    registry.Ai = new AI();
    registry.Ai.selectImage();
    ///////////

    var poolWidth = $("#questionsPool").width();
    var poolLeft = $("#questionsPool").offset().left;
    $("#selectContainer").css('left', poolLeft + poolWidth + 50);
});


function AddEventListeners() {

    $(".question").not(".grayscale").bind('click touchstart', selectQuestion);
    $("#cancelButton").on('click', deselectQuestion);
    $("#selectButton").on('click', makeQuestion);
    enableCrosshair(true);
    $(".card").on("click touchstart", cardSelected);
    $(".resetAll").on("click", reset);

    window.onresize = function () {
        var poolWidth = $("#questionsPool").width();
        var poolLeft = $("#questionsPool").offset().left;
        $("#selectContainer").css('left', poolLeft + poolWidth + 50);
    };

    window.onbeforeunload = function () {
        store.clear();
        //return "Dude, are you sure you want to leave and forfeit the fight!?";
    };

    window.onunload = function () {
        store.clear();
    };

}

function makeQuestion(e) {
    // TODO:CHECK IF USER HAS SELECTED QUESTION //
    var item = $(".selectedQuestion").attr("rel");

    console.log(($("#questionsCounter").children(".good").length + $("#questionsCounter").children(".nogood").length));
    if (($("#questionsCounter").children(".good").length + $("#questionsCounter").children(".nogood").length) >= 5) {
        console.log("GAME OVER DUDE!");
        $("#modalPickEnabler").leanModal({
            top: 300
        }).trigger('click');
        return false;
    }

    var result = registry.Ai.answerQuestion(item);

    if (result === undefined) {
        return false;
    }


    if (result === false) {
        var questionMark = $("#questionsCounter").children();

        ////////////////////////////
        for (var i = 0; i < questionMark.length; i++) {

            if ($(questionMark[i]).hasClass("nogood") || $(questionMark[i]).hasClass("good")) {

            } else {
                $(questionMark[i]).addClass("nogood").attr("src", "assets/nogood.png");
                $(".selectedQuestion").off();
                $(".selectedQuestion").addClass("grayscale").attr("disabled", "disabled").removeClass("selectedQuestion");

                break;
            }
        }
        /////////////////////////////
        // showing which cards are excluded //
        checkCards(questionToProp(item), false);
    } else {
        var questionMark = $("#questionsCounter").children();
        ////////////////////////////
        for (var i = 0; i < questionMark.length; i++) {

            if ($(questionMark[i]).hasClass("nogood") || $(questionMark[i]).hasClass("good")) {

            } else {
                $(questionMark[i]).addClass("good").attr("src", "assets/good.png");
                $(".selectedQuestion").off();
                $(".selectedQuestion").addClass("grayscale").attr("disabled", "disabled").removeClass("selectedQuestion");

                break;
            }
        }
        /////////////////////////////
        // showing which cards are excluded //
        checkCards(questionToProp(item), true);
    }

}

function deselectQuestion() {
    $(".question").removeClass("selectedQuestion");
}

function selectQuestion(e) {
    console.log(e);
    var item = e.currentTarget;
    $(".question").removeClass("selectedQuestion");
    $(item).addClass("selectedQuestion");

}

function createCards() {

    $("#cardCase").empty();
    for (var i = 0; i < 26; i++) {
        var card = new Card(properties[i], i);
        card = card.createHTML();
        registry.cards.push(card.item);
        $("#cardCase").append(card.html);

    }
}

function checkCards(prop, reverse) {

    for (var i = 0; i < registry.cards.length; i++) {
        var item;
        var card = registry.cards[i];
        // reverse means !prop //
        if (reverse === true) {
            if (card.checkProperty(prop) === false) {
                item = $("#cardCase").children().get(i);
                $(item).addClass("grayscale");
            }
        } else {
            if (card.checkProperty(prop) === true) {
                item = $("#cardCase").children().get(i);
                $(item).addClass("grayscale");
            }
        }
    }

}

function cardSelected(e) {
    var item = e.currentTarget;
    var img = $(item).children(".cardImg").attr("src");
    var path = $(item).children(".cardImg").attr("src");
    img = String(img).split("/");
    img = img[1];

    //console.log(img, registry.Ai.getSelectedImageFile());

    /// player guessed right ///
    if (img === registry.Ai.getSelectedImageFile()) {
        $(".winPick").attr('src', path);
        //TweenMax.to(item,0.8,{scaleX:2, scaleY:2});
        $("#modalWinEnabler").leanModal({
            top: 300
        }).trigger('click');

        var wins = store.get("wins");
        if (wins === undefined || wins === null) {
            wins = 0;
        }
        store.set("wins", wins + 1);

        $("#scoreText>span").text(store.get("wins"));
    } else {
        path = "assets/" + registry.Ai.getSelectedImageFile();
        $(".lossPick").attr('src', path);
        //TweenMax.to(item,0.8,{scaleX:2, scaleY:2});
        $("#modalLossEnabler").leanModal({
            top: 300
        }).trigger('click');
        reset(false);
    }

}

function createQuestions() {
    $("#questionsScroller").empty();
    for (var i = 0; i < questions.length; i++) {
        var item = "<div class='question' rel='" + questions[i].key + "'>" + questions[i].question + "</div>";
        $("#questionsScroller").append(item);
    }
}

function createThrowprops() {
    var gridWidth = 800,
        gridHeight = 256,
        realHeight = $("#questionsScroller").height()+20,
        gridRows = 2,
        gridColumns = 1;

    TweenLite.set($("#questionsPool"), {
        height: gridHeight,
        width: gridWidth
    });
    TweenLite.set($("#questionsScroller"), {
        height: realHeight,
        lineHeight: realHeight
    });

    Draggable.create("#questionsScroller", {
        bounds: $("#questionsPool"),
        edgeResistance: 0.65,
        type: "y",
        resistance: 5,
        velocity: "auto",
        throwProps: true,
        dragClickables: true,
        onThrowUpdate: function () {
            $(".arrow").hide(400);
        },

        snap: {
            x: function (endValue) {
                return Math.round(endValue / gridWidth) * gridWidth;
            }

        }
    });

}

/////// custom cursor /////////////


function enableCrosshair(simple) {

    if (simple === true) {
        $("#content").bind('mouseenter', function () {
            $('#simpleCrosshair').css('display', 'block');
        });

        $(document).bind('mousemove', function (e) {
            //console.log(e,e.clientX - 20, e.clientY - 40);
            $('#simpleCrosshair').css('display', 'block').css('left', e.clientX - 20).css('top', e.clientY - 40);
        });
    } else {
        $("#content").bind('mouseenter', function () {
            $('#mycursor').css('display', 'block');
        });

        $(document).bind('mousemove', function (e) {
            //console.log(e,e.clientX - 20, e.clientY - 40);
            $('#mycursor').css('display', 'block').css('left', e.clientX - 20).css('top', e.clientY - 40);
        });
    }
}

function removeCrosshair() {
    $("#content").unbind('mouseenter');

    $(document).unbind('mousemove');
    $("#simpleCrosshair").css('display', 'none');
}

function reset(e) {
    TweenMax.to($("#lean_overlay"), 0.7, {
        "opacity": 1
    });

    TweenMax.delayedCall(0.7, rebuild, [e]);

}

function rebuild(e) {
    $(".card").removeClass("grayscale");
    $(".selectedQuestion").removeClass("grayscale");
    $(".question").removeClass("selectedQuestion");

    $(".questionMark").removeClass("good").removeClass("nogood");
    $(".questionMark").attr("src", "assets/questionMark.png");
    createCards();
    createQuestions();
    AddEventListeners();
    registry.Ai = new AI();
    registry.Ai.selectImage();

    TweenMax.to("#lean_overlay", 0.8, {
        "opacity": 0.5,
        onComplete: function () {

            if (e !== false) {
                $("#lean_overlay").fadeOut(500);
                TweenMax.to(".modal", 0.8, {
                    'autoAlpha': 0,
                    onComplete: function () {
                        $(".modal").css({
                            "display": "none",
                            "visibility": "visible",
                            "opacity": 1
                        });
                    }
                });

            }

        },
        onCompleteParams: [e]
    });


}

function questionToProp(query) {
    var _query = String(query).replace(/[\s\?]/g, '');

    return _query;
}